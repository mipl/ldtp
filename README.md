# Local Directional Texture Pattern

## Description

This code presents a local texture code that uses the principal directional information, and the texture information in the two principal directions in the local neighborhood being analized.

## Usage

To code a given image you can use this code by

```matlab
% load image in I

% default invocation (using threshold 5, mask Kirsch, and neighborhood size of 3x3)
C = ldtp(I);

% using threshold 15, mask Sobel, and neighborhood size of 5x5)
C = ldtp(I,'thresh',15, 'mask', 'sobel', 'masksize', 5);
```

## Citation

If you use this code please cite

> A. Ramírez Rivera et al., Local Directional Texture Pattern image descriptor, Pattern Recognition Letters (2014), http://dx.doi.org/10.1016/j.patrec.2014.08.012

BibTeX:
```tex
@article{Ramirez2015,
title = "Local Directional Texture Pattern image descriptor ",
author = "Ram\'irez Rivera, Ad\'in and Rojas Castillo, Jorge and Chae, Oksam",
journal = "Pattern Recognition Letters ",
volume = "51",
pages = "94--100",
year = "2015",
issn = "0167-8655",
doi = "http://dx.doi.org/10.1016/j.patrec.2014.08.012",
url = "http://www.sciencedirect.com/science/article/pii/S0167865514002724"
}
```